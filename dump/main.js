const textArea = document.querySelector(".editor");
let currentSelection = 0;

function tabRight(event) {
  currentSelection = this.selectionStart;
  console.log(this.selectionStart, this.selectionEnd);
  if (event.keyCode === 9) {
    event.preventDefault();
    let s = this.selectionStart;
    this.value =
      this.value.substring(0, this.selectionStart) +
      "\t" +
      this.value.substring(this.selectionEnd);
    this.selectionEnd = s + 1;
    currentSelection = this.selectionEnd;
  }
}

textArea.onkeydown = tabRight;

const tabRightButton = document.getElementById("tab_right");
tabRightButton.onclick = function () {
  let s = textArea.selectionStart;
  textArea.value =
    textArea.value.substring(0, textArea.selectionStart) +
    "\t" +
    textArea.value.substring(textArea.selectionEnd);
  textArea.focus();
  textArea.selectionEnd = s + 1;
};

const todo = document.getElementById("todo");
todo.onclick = function () {
  let s = textArea.selectionStart;
  let str = "<button>afk</button>";
  textArea.value =
    textArea.value.substring(0, textArea.selectionStart) +
    str +
    textArea.value.substring(textArea.selectionEnd);
  textArea.focus();
  textArea.selectionEnd = s + str.length;
};
