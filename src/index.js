const divInstall = document.getElementById("installContainer");
const butInstall = document.getElementById("butInstall");

window.addEventListener("beforeinstallprompt", function (event) {
  window.deferredPrompt = event;
  divInstall.classList.toggle("hidden", false);
});
butInstall.addEventListener("click", function () {
  const promptEvent = window.deferredPrompt;
  if (!promptEvent) {
    // The deferred prompt isn't available.
    return;
  }
  // Show the install prompt.
  promptEvent.prompt();
  promptEvent.userChoice.then(function (result) {
    console.log(result);
    window.deferredPrompt = null;
    // Hide the install button.
    divInstall.classList.toggle("hidden", true);
  });
});

window.addEventListener("appinstalled", (event) => {
  console.log("👍", "appinstalled", event);
  // Clear the deferredPrompt so it can be garbage collected
  window.deferredPrompt = null;
});

if ("serviceWorker" in navigator) {
  navigator.serviceWorker.register("./sw.js");
}
